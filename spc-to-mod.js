const commandLineArgs = require("command-line-args");
const optionDefinitions = [
	{name: "file", alias: "f", type: String, defaultOption: true},
	{name: "pattern-offset", alias: "p", type: String},
	{name: "instrument-offset", alias: "i", type: String},
	{name: "no-samples", alias: "S", type: Boolean}
];
const options = commandLineArgs(optionDefinitions);

if(!options.file) {
	console.log("spc-to-mod by Ivanq (c) 2017");
	console.log("");
	console.log("Usage: node spc-to-mod.js file [--pattern-offset offset] [--instrument-offset offset] [--no-samples]")
	console.log("Converts file.spc and file.ins to file.mod");
	console.log("");
	console.log("file --file -f          Filename (+.spc and +.ins) to convert");
	console.log("--pattern-offset -p     Offset for patterns in memory. Default: 0o40000");
	console.log("--instrument-offset -i  Offset for instruments in memory. Default: 0o4000");
	console.log("--no-samples -S         Don't write instruments to MOD. Default: false");
	process.exit(0);
}

let baseName = options.file;
baseName = baseName.replace(/\.spc$/i, "");
baseName = baseName.replace(/\.ins$/i, "");

const SPCParse = require("./spcparse");
const ModLib = require("./modlib");
const fs = require("fs");

let spcStream = fs.createReadStream(baseName + ".spc");
spcStream.once("readable", () => {
	const parser = new SPCParse.SPCParser(spcStream, options["pattern-offset"], options["instrument-offset"]);
	const insParser = new SPCParse.INSParser(baseName + ".ins");

	if(parser.title.length > 20) {
		console.error("Title \"" + parser.title + "\" was truncated to 20 bytes");
	}

	let instrumentListWithoutOffset = (
		Object.keys(parser.instrumentList)
			.filter(instrumentId => !parser.instrumentList[instrumentId].sst1Offset)
			.map(instrumentId => parseInt(instrumentId))
	);

	let patterns = parser.patterns.map(pattern => {
		let rows = pattern.map(row => {
			return row.map(note => {
				if(note.noteStop) {
					return {
						empty: true,
						effect: "volume0"
					};
				} else if(!note.newNote) {
					return {
						empty: true,
						effect: null
					};
				} else {
					let instrumentId = note.instrumentId;
					let effect = null;

					let instrument = parser.instrumentList[instrumentId];
					if(instrument.sst1Offset) {
						instrumentId = instrument.sst1Parent;
						effect = "sampleOffset" + instrument.sst1Offset;
					}

					instrumentId = instrumentListWithoutOffset.indexOf(instrumentId);

					return {
						sign: note.note,
						instrument: instrumentId + 1,
						effect: effect
					};
				}
			}).concat([
				{
					empty: true,
					effect: null
				}
			]);
		});
		if(rows.length < 64) {
			rows[rows.length - 1][3].effect = "patternBreak";
		}

		return rows;
	});
	patterns[parser.patternSeq[0]][0][3].effect = "setSpeed" + Math.round(182000 / parser.tempo);

	patterns = patterns.map(rows => {
		return new ModLib.Pattern({
			rows: rows
		});
	});

	const song = new ModLib.Song({
		title: parser.title.substr(0, 20),
		samples: options["no-samples"] ? [] : (
			instrumentListWithoutOffset
				.map(instrumentId => {
					let address = parser.instrumentList[instrumentId].instrument;
					let size = parser.endOfInstrument[parser.instrumentList[instrumentId].instrument] - address;
					let name = Object.keys(parser.instrumentList).length == parser.instruments.length ? parser.instruments[instrumentId] : "Instrument " + instrumentId;

					return new ModLib.Sample({
						size: size,
						title: name,
						buffer: insParser.load(address, size),
						finetune: -17
					});
				})
		),
		patternSeq: parser.patternSeq,
		patterns: patterns
	});

	fs.writeFileSync(baseName + ".mod", song.encode().getBuffer());

	console.log("Done. Wrote to " + baseName + ".mod");
});