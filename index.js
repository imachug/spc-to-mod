/*
const ModLib = require("./modlib");

let song = new ModLib.Song({
	title: "Sample Song",
	samples: [],
	patternSeq: [0, 1],
	patterns: [
		new ModLib.Pattern({
			rows: [
				[{sign: "C-2", instrument: 2}, {sign: "C#2", instrument: 1}, null, null]
			]
		}),
		new ModLib.Pattern({
			rows: [
				[{sign: "D-1", instrument: 4}, {sign: "D-2", instrument: 5}, {sign: "F#2", instrument: 5}, {sign: "A#3", instrument: 5}]
			]
		})
	]
});

process.stdout.write(song.encode().getBuffer());
*/

const commandLineArgs = require("command-line-args");
const optionDefinitions = [
	{name: "pattern-offset", alias: "p", type: String},
	{name: "instrument-offset", alias: "i", type: String},
	{name: "help", alias: "h", type: Boolean}
];
const options = commandLineArgs(optionDefinitions);

if(options.help) {
	console.log("spc-to-mod by Ivanq (c) 2017");
	console.log("");
	console.log("Usage: node index.js [--pattern-offset offset] [--instrument-offset offset]")
	console.log("Debug SPC");
	console.log("");
	console.log("--pattern-offset -p     Offset for patterns in memory. Default: 0o40000");
	console.log("--instrument-offset -i  Offset for instruments in memory. Default: 0o4000");
	process.exit(0);
}

const SPCParse = require("./spcparse");
const SPCParser = SPCParse.SPCParser;

process.stdin.once("readable", () => {
	const parser = new SPCParser(process.stdin, options["pattern-offset"], options["instrument-offset"]);
	delete parser.stream;

	function pad(s) {
		if(typeof s == "boolean") {
			return s ? "+" : " ";
		} else if(typeof s == "undefined") {
			return pad("-");
		}
		return s.toString() + " ".repeat(8 - s.toString().length);
	}
	function padX(s) {
		if(typeof s == "boolean") {
			return s ? "+" : " ";
		} else if(typeof s == "undefined") {
			return pad("-");
		} else if(typeof s == "number") {
			return "0".repeat(2 - s.toString().length) + s.toString();
		}
		return s + " ".repeat(3 - s.toString().length);
	}

	console.log(parser.title);
	console.log("Tempo:", parser.tempo);
	console.log("Pattern sequence:", parser.patternSeq.join(" "));
	console.log("Instruments:");

	let j = 0;
	Object.keys(parser.instrumentList).forEach(i => {
		if(parser.instrumentList[i].sst1Offset) {
			console.log(
				"    <", i, ">",
				parser.instrumentList[i].instrument + "+" + parser.instrumentList[i].sst1Offset,
				"-sst1offset-",
				parser.endOfInstrument[parser.instrumentList[i].instrument]
			);
		} else {
			console.log(
				"    <", i, ">",
				parser.instrumentList[i].instrument,
				parser.instruments[j] || "-noname-",
				parser.endOfInstrument[parser.instrumentList[i].instrument]
			);
			j++;
		}
	});

	parser.patterns.forEach((pattern, i) => {
		console.log("Pattern #" + i, "@", parser.patternList[i]);
		pattern.forEach((row, j) => {
			console.log(
				padX(j),

				"|",

				padX(row[0].noteStop ? "=" : !row[0].newNote ? "" : row[0].note),
				pad(row[0].instrumentId == -1 ? "" : row[0].instrumentId),
				pad(row[0].instrument),

				"|",

				padX(row[1].noteStop ? "=" : !row[1].newNote ? "" : row[1].note),
				pad(row[1].instrumentId == -1 ? "" : row[1].instrumentId),
				pad(row[1].instrument),

				"|",

				padX(row[2].noteStop ? "=" : !row[2].newNote ? "" : row[2].note),
				pad(row[2].instrumentId == -1 ? "" : row[2].instrumentId),
				pad(row[2].instrument)
			);
		});
	});
});