const assert = require("assert");

const COMMAND_PLAY = 38209;
const COMMAND_STOP = 37697;

const NOTES = [
	[0,     "D-0"],
	[0,     "D#0"],
	[0,     "E-0"],
	[0,     "F-0"],
	[0,     "F#0"],
	[0,     "G-0"],
	[0,     "G#0"],
	[0,     "A-0"],
	[0,     "A#0"],
	[0,     "B-0"],
	[16384, "C-1"],
	[17408, "C#1"],
	[18432, "D-1"],
	[19456, "D#1"],
	[20480, "E-1"],
	[21845, "F-1"],
	[23210, "F#1"],
	[24576, "G-1"],
	[25941, "G#1"],
	[27306, "A-1"],
	[29013, "A#1"],
	[30720, "B-1"],
	[32768, "C-1"],
	[34816, "C#2"],
	[36864, "D-2"],
	[38912, "D#2"],
	[40960, "E-2"],
	[43691, "F-2"],
	[46421, "F#2"],
	[49152, "G-2"],
	[51883, "G#2"],
	[54613, "A-2"],
	[58027, "A#2"],
	[61440, "B-2"],
	[65535, "C-3"],
	[0,     "D-4"],
	[0,     "D#4"],
	[0,     "E-4"],
	[0,     "F-4"],
	[0,     "F#4"],
	[0,     "G-4"],
	[0,     "G#4"],
	[0,     "A-4"],
	[0,     "A#4"],
	[0,     "B-4"]
];
const NOTE_OFFSET = -6;
const MAX_NOTE_ERROR = 8;

function from16(bytes) {
	return bytes[0] | (bytes[1] << 8);
}

module.exports = {
	from16,
	assert,
	COMMAND_PLAY,
	COMMAND_STOP,
	NOTES,
	NOTE_OFFSET,
	MAX_NOTE_ERROR
};