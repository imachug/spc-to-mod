const assert = require("assert");

const RESTART_BYTE = 127;
const FINETUNE_TABLE = [0, 1, 2, 3, 4, 5, 6, 7, -8, -7, -6, -5, -4, -3, -2, -1];
const SAMPLE_VOLUME = 0x40;
const NOTES = {
	"C":  [1712, 856, 428, 214, 107],
	"C#": [1616, 808, 404, 202, 101],
	"D":  [1525, 762, 381, 190, 95 ],
	"D#": [1440, 720, 360, 180, 90 ],
	"E":  [1357, 678, 339, 170, 85 ],
	"F":  [1281, 640, 320, 160, 80 ],
	"F#": [1209, 604, 302, 151, 76 ],
	"G":  [1141, 570, 285, 142, 71 ],
	"G#": [1077, 538, 269, 135, 67 ],
	"A":  [1017, 508, 254, 127, 64 ],
	"A#": [961,  480, 240, 120, 60 ],
	"B":  [907,  453, 226, 113, 57 ]
}

function padSpace(str, size) {
	return str + " ".repeat(size - str.length);
}
function padNull(str, size) {
	return str + "\0".repeat(size - str.length);
}

function to32(num) {
	let buf = new Buffer(4);
	buf[0] = num >> 24;
	buf[1] = (num >> 16) & 0xFF;
	buf[2] = (num >> 8) & 0xFF;
	buf[3] = num & 0xFF;
	return buf;
}
function to16(num) {
	let buf = new Buffer(2);
	buf[0] = num >> 8;
	buf[1] = num & 0xFF;
	return buf;
}

module.exports = {
	padSpace,
	padNull,
	to16,
	to32,
	assert,
	RESTART_BYTE,
	FINETUNE_TABLE,
	SAMPLE_VOLUME,
	NOTES
};