const DynamicBuffer = require("DynamicBuffer");
const util = require("./util");
const Sample = require("./sample");

class Song {
	constructor(info) {
		this.title = info.title;
		this.samples = info.samples;
		this.patternSeq = info.patternSeq;
		this.patterns = info.patterns;

		util.assert(this.title.length <= 20, "Title size must be 0..20 bytes");
		util.assert(this.patternSeq.length >= 1 && this.patternSeq.length <= 128, "Pattern sequence must be 1..128");
		util.assert(this.patterns.length <= 63, "Patterns must be 0..63");
		util.assert(this.patterns.length == Math.max.apply(Math, this.patternSeq) + 1, "Pattern sequence out of patterns");
	}

	encode() {
		let song = new DynamicBuffer();
		song.append(util.padNull(this.title, 20));

		this.samples.forEach(sample => {
			song.concat(sample.encode());
		});
		for(let i = this.samples.length; i < 31; i++) {
			song.concat((new Sample({
				title: "",
				size: 0
			})).encode());
		}

		song.write(this.patternSeq.length);
		song.write(util.RESTART_BYTE);
		this.patternSeq.forEach(patternNum => {
			song.write(patternNum);
		});
		for(let i = this.patternSeq.length; i < 128; i++) {
			song.write(0);
		}

		song.append("M.K.");

		this.patterns.forEach(pattern => {
			song.concat(pattern.encode());
		});

		this.samples.forEach(sample => {
			song.concat(sample.encodeData());
		});

		return song;
	}
};

module.exports = Song;